# Azure cloud infrastracture project

## Schemas

![photo_2021-10-28_13-03-21](/uploads/91010b7ffdf4b5b5ac63d6f0b1b0142a/photo_2021-10-28_13-03-21.jpg)

## Description

Current repository, containing:  
-terraform files (for deploying instances in the cloud)  

Remote desktop to pull & deploy cloud infrastracture
(including instance with Docker, instance with Jenkins, instance withmonitoring stack)

## Usage

### Terraform

```bash
az login
terraform init
terraform validate
terraform apply
terraform destroy
```
